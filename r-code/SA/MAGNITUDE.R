#============================================
# 1) importation of transformed data 
#============================================
library(dplyr)
library(ggplot2)
library(tidyr)
library(tidyverse)
library(purrr)
library(nlme)
library(tibble)
library(broom)
library(car)
library(apaTables)
library(ggrepel)
library(sjPlot)


source(here::here("DATA_SA.R"))

str(data_sa)

# coding variable
data_sa_group <- data_sa%>% dplyr::select(score_abs, subject,
                                          handedness_c, handedness,
                                          group_c,group,
                                          session, session_c,
                                          hand_c, hand, item) %>% 
  
  #dplyr::filter(!is.na(subject)) 
  unique(.) %>% 
  na.omit(.)

average = coef(lmList(score_abs ~ 1 | subject, data_sa_group)) # average of within subject variable
session_diff = coef(lmList(score_abs ~ 0 + session_c | subject, data_sa_group))
hand_diff = coef(lmList(score_abs~ 0 + hand_c | subject, data_sa_group))
session_hand_diff = coef(lmList(score_abs ~ 0 + session_c:hand_c | subject, data_sa_group))
item_diff = coef(lmList(score_abs ~ 0 + item | subject, data_sa_group))
item_session_diff = coef(lmList(score_abs ~ 0 + item:session_c | subject, data_sa_group))
item_session_hand_diff = coef(lmList(score_abs ~ 0 + item:session_c:hand_c | subject, data_sa_group))

fit <-
  cbind(average, session_diff, hand_diff, session_hand_diff, item_session_hand_diff) %>% 
  rownames_to_column("subject") %>% mutate(subject = as.numeric(subject)) %>% 
  rename(intercept = `(Intercept)`)  
#============================================
# 2) Inférence approche qualitative 
#============================================

bl<-data_sa_group[data_sa_group$session == "pretest", ]
# bl = baseline
summary(lm(score_abs ~ group_c + handedness_c, bl)) # ok.
# bl = baseline
summary(lm(score_abs ~ group_c + handedness_c, bl)) # *. 

source(here::here("SA","CLEAN_FIT.R"))
clean_fit(lm = lm(score_abs ~ group_c + handedness_c, bl), data = bl)

plot(score_abs ~  group_c, new_data, main = "Pretest performance", ylab = "Standard deviation")
abline(lm(score_abs ~ group_c, new_data))

# on a 2 groupes qui ne sont pas équivalents à la base : peut expliquer la régression 
# vers la moyenne chez ce groupe . attt à l'interprétation ! 


#=======================================
fit2<-data_sa_group %>% 
  dplyr::select(subject, group_c, handedness_c) %>% 
  inner_join(fit, by = "subject") %>% 
  unique(.)

reg = lm (session_c ~ group_c + handedness_c, fit2)

# Outliers
clean_fit(lm = reg, data = fit2) # fonction maison qui repère outlier et les enlève 
new_reg<- `lm_session_c ~ group_c + handedness_c`

# Assomption 
plot(new_reg, which = 2)
hist(rstandard(new_reg), prob = T)
lines(density(rstandard(new_reg)), col="blue", lwd=2)
plot(new_reg, which = 3)


# Les conditions d'applications sont respectees.

# Taille d'effet
apaTables::apa.aov.table(new_reg)
# on a une énorme taille d'effet à .19, mais intervalle de confiance large
#Résultat des autres régressions, sans screener les outliers ni les conditions d'applications : 


#=================================
# Graphique de l'interaction
#=================================

data_sa_mean <- 
  data_sa_group %>% 
  dplyr::filter(!subject %in% outlier_name) %>% 
  dplyr::group_by(session, group) %>% 
  dplyr::summarize(mean = mean(score_abs), sd = sd(score_abs)) %>%
  ungroup() %>% 
  mutate (
    lower_sd = mean - sd,
    upper_sd = mean + sd
  )


pd <- position_dodge(0.5)
data_sa_mean$session <- factor(data_sa_mean$session,levels = c("pretest", "posttest"))

ggplot(data = data_sa_mean, aes(x = session, y = mean, ymin = lower_sd, ymax = upper_sd, 
                                colour = group)) +
  geom_point(size = 5, position = position_dodge(width = 0.2)) +
  geom_errorbar(position = position_dodge(width = 0.2), width = 0.1, size = 2) +
  scale_colour_manual(values = c("blue", "red")) +
  labs(y = "Ecart type", x = "Groupe")



data_sa_mean_hand <- 
  data_sa_group %>% 
  dplyr::filter(!subject %in% outlier_name) %>% 
  dplyr::group_by(session, group, hand) %>% 
  dplyr::summarize(mean = mean(score_abs), sd = sd(score_abs)) %>%
  ungroup() %>% 
  mutate (
    lower_sd = mean - sd,
    upper_sd = mean + sd
  )


data_sa_mean_hand$session <- factor(data_sa_mean_hand$session,levels = c("pretest", "posttest"))
ggplot(data = data_sa_mean_hand, aes(x = session, y = mean, ymin = lower_sd, ymax = upper_sd, 
                                     colour = group)) +
  geom_point(size = 4, position = position_dodge(width = 0.2)) +
  geom_errorbar(position = position_dodge(width = 0.2), width = 0.1, size = 1) +
  scale_colour_manual(values = c("blue", "red")) +
  labs(y = "Absolute Mean", x = "Session")+
  facet_wrap(~ hand)


mean<-c(data_sa_mean$mean[1] - data_sa_mean$mean[3], data_sa_mean$mean[2] - data_sa_mean$mean[4])
m2<-lm(session_c ~  group_c, new_data)
mu<-link( m2 ,data=data.frame(group=-0.5:0.5) )
mu.2 <- apply( mu , 2 , mean )
mu.pi2 <- apply( mu , 2 , PI )


par(mar=c(5,4,2,30)+0.1)
plot(new_data$group_c, new_data$session_c  ,
     col=ifelse(new_data$group_c > 0  , "red","blue") , # red = pert
     xlim = c(-0.6,0.6),
     xaxt="n" , 
     ylab="Absolute Mean" ,
     xlab="Group",
     main = "Absolute Mean Gain Score as a Function of Group ",
     cex.lab = 1.3)
lines( -0.5:0.5 , mu.2 , lwd = 1.7 )
points(-0.5:0.5, mean, bg=ifelse(new_data$group_c > 0  , "red","blue"), pch=24, cex=2)
axis( 1 , at=c(-0.5,0.5) , labels=c("Control","Perturbation") )
abline(0,0, lty = "dashed", col = "grey")
shade(mu.pi2, -0.5:0.5, col=col.alpha(rangi2,0.2))
legend('top', legend=c("Control", "Perturbation"),
       col=c("blue", "red"),lty = 1:1, cex=1.2)



fit2 %>% 
  as_tibble() %>% 
  tidyr::gather(key = term, value = vd, -c(subject, group_c)) %>% 
  group_by(term) %>% 
  nest() %>% 
  mutate(
    fit = purrr::map(data, ~ lm(vd ~ 1 + group_c, data = .x)),
    tidied = purrr::map(fit, tidy)
  ) %>% 
  ungroup() %>% 
  unnest(term, tidied) %>% 
  mutate(p.value  = round(p.value, 5)) %>% 
  dplyr::select(term, term1, estimate, statistic, p.value)



#============================================
# 3) Inference approche quantitative 
#============================================

fit3<-data_sa_distance %>% 
  dplyr::select(subject, tt_score, handedness_c) %>% 
  inner_join(fit, by = "subject") %>% 
  unique(.)

reg = lm (session_c ~ tt_score + handedness_c, fit3)

# Outliers
clean_fit(lm = reg, data = fit3) # fonction maison qui repère outlier et les enlève 
# On a supprimé le participant 12   
new_reg<- `lm_session_c ~ tt_score + handedness_c`

# Assomption 

plot(new_reg, which = 2)
hist(rstandard(new_reg), prob = T)
lines(density(rstandard(new_reg)), col="blue", lwd=2)
plot(new_reg, which = 3)

# Les conditions d'applications sont respectées. 

# Taille d'effet
apaTables::apa.aov.table(new_reg)

# Résultat des autres régressions, sans screener les outliers ni les conditions d'applications : 

fit3 %>% 
  as_tibble() %>% 
  tidyr::gather(key = term, value = vd, -c(subject, tt_score)) %>% 
  group_by(term) %>% 
  nest() %>% 
  mutate(
    fit = purrr::map(data, ~ lm(vd ~ 1 + tt_score, data = .x)),
    tidied = purrr::map(fit, tidy)
  ) %>% 
  ungroup() %>% 
  unnest(term, tidied) %>% 
  mutate(p.value  = round(p.value, 5)) %>% 
  dplyr::select(term, term1, estimate, statistic, p.value)

